
package com.nogroup.municipality.booster.models;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.sun.codemodel.JCodeModel;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "groupId",
    "artifactId",
    "version",
    "archetype"
})

@JsonTypeInfo(
  use = JsonTypeInfo.Id.NAME, 
  include = JsonTypeInfo.As.PROPERTY, 
  property = "archetype")
@JsonSubTypes({ 
  @Type(value = MultiModuleProject.class, name = "MULTI_MODULE"), 
})
public abstract class Project {

    @JsonProperty("groupId")
    private String groupId;
    @JsonProperty("artifactId")
    private String artifactId;
    @JsonProperty("version")
    private String version;
    @JsonProperty("archetype")
    private ProjectArchetypes archetype;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    @JsonIgnore
	private JCodeModel cm = new JCodeModel();

    @JsonProperty("groupId")
    public String getGroupId() {
        return groupId;
    }

    @JsonProperty("groupId")
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Project withGroupId(String groupId) {
        this.groupId = groupId;
        return this;
    }

    @JsonProperty("artifactId")
    public String getArtifactId() {
        return artifactId;
    }

    @JsonProperty("artifactId")
    public void setArtifactId(String artifactId) {
        this.artifactId = artifactId;
    }

    public Project withArtifactId(String artifactId) {
        this.artifactId = artifactId;
        return this;
    }

    @JsonProperty("version")
    public String getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    public Project withVersion(String version) {
        this.version = version;
        return this;
    }

    @JsonProperty("archetype")
    public ProjectArchetypes getArchetype() {
        return archetype;
    }

    @JsonProperty("archetype")
    public void setArchetype(ProjectArchetypes archetype) {
        this.archetype = archetype;
    }

    public Project withArchetype(ProjectArchetypes archetype) {
        this.archetype = archetype;
        return this;
    }
    
    
    public JCodeModel getCm() {
		return cm;
	}

	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Project withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(groupId).append(artifactId).append(version).append(archetype).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Project) == false) {
            return false;
        }
        Project rhs = ((Project) other);
        return new EqualsBuilder().append(groupId, rhs.groupId).append(artifactId, rhs.artifactId).append(version, rhs.version).append(archetype, rhs.archetype).append(additionalProperties, rhs.additionalProperties).isEquals();
    }
    
    public abstract void build();
    
    public String rootPackage() {
    	return this.groupId + "." + this.artifactId ;
    }
    
    public String rootPath() {
    	return "/src/main/java/" + (this.groupId + "." + this.artifactId).replace(".", "/") ;
    }
}
