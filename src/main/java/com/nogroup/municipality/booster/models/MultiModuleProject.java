package com.nogroup.municipality.booster.models;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "groupId",
    "artifactId",
    "version",
    "archetype",
    "dataTypes",
    "modules"
})
public class MultiModuleProject extends Project{

	@JsonProperty("modules")
    private List<Module> modules = new ArrayList<Module>();
	
	@JsonProperty("dataTypes")
    private DataTypes dataTypes;
	
	@JsonProperty("modules")
    public List<Module> getModules() {
        return modules;
    }

    @JsonProperty("modules")
    public void setModules(List<Module> modules) {
        this.modules = modules;
    }

    public Project withModules(List<Module> modules) {
        this.modules = modules;
        return this;
    }

    @JsonProperty("dataTypes")
    public DataTypes getDataTypes() {
        return dataTypes;
    }

    @JsonProperty("dataTypes")
    public void setDataTypes(DataTypes dataTypes) {
        this.dataTypes = dataTypes;
    }

    public Project withDataTypes(DataTypes dataTypes) {
        this.dataTypes = dataTypes;
        return this;
    }
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getGroupId()).
        		   append(getArtifactId()).append(getVersion()).
        		   append(getArchetype()).append(modules).
        		   append(getAdditionalProperties()).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof MultiModuleProject) == false) {
            return false;
        }
        MultiModuleProject rhs = ((MultiModuleProject) other);
        return new EqualsBuilder().append(getGroupId(), rhs.getGroupId())
        		.append(getArtifactId(), rhs.getArtifactId()).append(getVersion(), rhs.getVersion())
        		.append(getArchetype(), rhs.getArchetype()).append(getModules(), rhs.getModules())
        		.append(getAdditionalProperties(), rhs.getAdditionalProperties()).isEquals();
    }

	@Override
	public void build() {
		
	}
	
    
}
