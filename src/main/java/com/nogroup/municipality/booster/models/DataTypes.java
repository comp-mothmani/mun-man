
package com.nogroup.municipality.booster.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "enumerated",
    "embedded",
    "entities"
})
public class DataTypes {

    @JsonProperty("enumerated")
    private List<Enumerated> enumerated = new ArrayList<Enumerated>();
    @JsonProperty("embedded")
    private List<Embedded> embedded = new ArrayList<Embedded>();
    @JsonProperty("entities")
    private List<Object> entities = new ArrayList<Object>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("enumerated")
    public List<Enumerated> getEnumerated() {
        return enumerated;
    }

    @JsonProperty("enumerated")
    public void setEnumerated(List<Enumerated> enumerated) {
        this.enumerated = enumerated;
    }

    public DataTypes withEnumerated(List<Enumerated> enumerated) {
        this.enumerated = enumerated;
        return this;
    }

    @JsonProperty("embedded")
    public List<Embedded> getEmbedded() {
        return embedded;
    }

    @JsonProperty("embedded")
    public void setEmbedded(List<Embedded> embedded) {
        this.embedded = embedded;
    }

    public DataTypes withEmbedded(List<Embedded> embedded) {
        this.embedded = embedded;
        return this;
    }

    @JsonProperty("entities")
    public List<Object> getEntities() {
        return entities;
    }

    @JsonProperty("entities")
    public void setEntities(List<Object> entities) {
        this.entities = entities;
    }

    public DataTypes withEntities(List<Object> entities) {
        this.entities = entities;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public DataTypes withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(enumerated).append(embedded).append(entities).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof DataTypes) == false) {
            return false;
        }
        DataTypes rhs = ((DataTypes) other);
        return new EqualsBuilder().append(enumerated, rhs.enumerated).append(embedded, rhs.embedded).append(entities, rhs.entities).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
