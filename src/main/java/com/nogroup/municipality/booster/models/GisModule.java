package com.nogroup.municipality.booster.models;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "artifact",
    "version",
    "archetype",
    "items",
    "views"
})
public class GisModule extends Module{

	@JsonProperty("items")
    private List<Item> items = new ArrayList<Item>();
    @JsonProperty("views")
    private List<View> views = new ArrayList<View>();
	
	@Override
	public void build() {
		
	}

	@JsonProperty("items")
    public List<Item> getItems() {
        return items;
    }

    @JsonProperty("items")
    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Module withItems(List<Item> items) {
        this.items = items;
        return this;
    }

    @JsonProperty("views")
    public List<View> getViews() {
        return views;
    }

    @JsonProperty("views")
    public void setViews(List<View> views) {
        this.views = views;
    }

    public Module withViews(List<View> views) {
        this.views = views;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getArtifact()).append(getVersion()).append(getArchetype())
        		.append(getItems()).append(getViews()).append(getAdditionalProperties()).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof GisModule) == false) {
            return false;
        }
        GisModule rhs = ((GisModule) other);
        return new EqualsBuilder().append(getArtifact(), rhs.getArtifact()).
        	   append(getVersion(), rhs.getVersion()).append(getArchetype(), rhs.getArchetype()).
        	   append(getItems(), rhs.getItems()).append(getViews(), rhs.getViews()).
        	   append(getAdditionalProperties(), rhs.getAdditionalProperties()).isEquals();
    }
    
    public void static_modules() {
    	
    }

}
