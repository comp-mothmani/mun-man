package com.nogroup.municipality.booster.models;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ItemPosition {
	TOOLBOX,
	MAIN,
	USER
}
