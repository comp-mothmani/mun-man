package com.nogroup.municipality.booster.builders;

import java.io.IOException;
import java.io.Serializable;

import com.google.common.reflect.ClassPath;

public class ResolveDataType implements Serializable{

	public static Class<?> find(String ss) throws IOException {
		final ClassLoader loader = Thread.currentThread().getContextClassLoader();

		for (final ClassPath.ClassInfo info : ClassPath.from(loader).getAllClasses()) {
			if (info.getName().endsWith("." + ss)) {
				final Class<?> clazz = info.load();
				return clazz;
			}
		}
		
		
		return new StaticResolve().get(ss);
	}

	public static Class<?> findInUtil(String ss) throws IOException {

		final ClassLoader loader = Thread.currentThread().getContextClassLoader();

		for (final ClassPath.ClassInfo info : ClassPath.from(loader).getTopLevelClasses()) {
			if (info.getName().contains("MultiLangName")) {
				System.out.println("" + info.load().getName());
			}
			if (info.getName().endsWith("." + ss) && info.getName().startsWith("java.util.")) {
				final Class<?> clazz = info.load();
				return clazz;
			}
		}
		return null;
	}


}
