package com.nogroup.municipality.booster.builders;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.nogroup.municipality.booster.models.MultiModuleProject;
import com.sun.codemodel.JClass;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JFieldVar;
import com.sun.codemodel.JForEach;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JPackage;
import com.sun.codemodel.JVar;
import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.data.HasValue.ValueChangeListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class BuildGenericTokenField {

	public static void run(MultiModuleProject prj) throws JClassAlreadyExistsException {
		JPackage jp = prj.getCm()._package(prj.rootPackage() + ".ccmp.generics");
		JDefinedClass jc = jp._class("TokenField");
		jc.javadoc().add("Generated by DevBooster Module.\n");
		jc.javadoc().add("Archetype   : Custom Component class << TokenField >>\n");
		jc.javadoc().add("Developer   : medzied.arbi@gmail.com\n");
		jc.javadoc().add("Date        : " + new Date().toGMTString() + "\n");
		
		JClass genericT = prj.getCm().ref("TOKEN_TYPE");
		JClass pp = prj.getCm().ref(ValueChangeListener.class).narrow(genericT);
		jc._extends(HorizontalLayout.class);
		jc._implements(pp);
		jc.generify("TOKEN_TYPE");
		
		//ComboBox
		JClass parameterType = prj.getCm().ref(ComboBox.class).narrow(genericT);
		JFieldVar cbx = jc.field(JMod.PRIVATE, parameterType, "cbx",JExpr._new(parameterType)) ;
		
		JFieldVar hznl = jc.field(
			JMod.PRIVATE, HorizontalLayout.class, "subContainer",
			JExpr._new(prj.getCm().ref(HorizontalLayout.class))) ;
		
		JClass ls = prj.getCm().ref("TOKEN_TYPE");
		JClass parameterTypeLs = prj.getCm().ref(List.class).narrow(ls);
		JFieldVar values = jc.field(
			JMod.PRIVATE, parameterTypeLs, "values",JExpr._new(prj.getCm().ref(ArrayList.class))) ;
		
		
		JMethod ini = jc.method(JMod.PRIVATE, prj.getCm().VOID, "init") ;
		ini.body().add(cbx.invoke("setEmptySelectionAllowed").arg(
			JExpr.lit(false))
		);
		
		ini.body().add(JExpr._this().invoke("addComponent").arg(cbx));
		ini.body().add(cbx.invoke("addValueChangeListener").arg(JExpr._this()));
		
		
		JMethod style = jc.method(JMod.PRIVATE, prj.getCm().VOID, "style") ;
		style.body().add(hznl.invoke("addStyleName").arg(
			JExpr.lit(ValoTheme.LAYOUT_WELL))
		);
		style.body().add(cbx.invoke("addStyleName").arg(
			JExpr.lit(ValoTheme.COMBOBOX_TINY))
		);
		style.body().add(cbx.invoke("setWidth").arg(
			JExpr.lit("100%"))
		);
		JMethod c = jc.constructor(JMod.PUBLIC) ;
		c.body().add(JExpr._this().invoke("init")) ;
		c.body().add(JExpr._this().invoke("style")) ;
		JMethod cc = jc.constructor(JMod.PUBLIC) ;
		cc.param(String.class, "caption");
		cc.body().add(JExpr._this().invoke("setCaption").arg(JExpr.lit("caption")));
		cc.body().add(JExpr._this().invoke("init")) ;
		cc.body().add(JExpr._this().invoke("style")) ;
		
		JMethod setter = jc.method(JMod.PUBLIC, prj.getCm().VOID, "set" + capitalizeFirst(values.name()));
        setter.param(values.type(), values.name());
        setter.body().assign(JExpr._this().ref(values.name()), JExpr.ref(values.name()));
        setter.javadoc().add("Set the " + values.name() + ".");
        setter.javadoc().addParam(values.name()).add("the new " + values.name());
        setter.body().add(cbx.invoke("setItems").arg(
			values)
		);
        
        JMethod getter = jc.method(JMod.PUBLIC, values.type(), "get" + capitalizeFirst(values.name()));
        getter.body()._return(values);
        getter.javadoc().add("Returns the " + values.name() + ".");
        getter.javadoc().addReturn().add(values.name());
        
        JMethod valueChange = jc.method(JMod.PUBLIC, prj.getCm().VOID, "valueChange");
        JClass g = prj.getCm().ref("TOKEN_TYPE");
		JClass ppp = prj.getCm().ref(ValueChangeEvent.class).narrow(genericT);
        JVar par = valueChange.param(ppp, "event");
        
		
		JForEach foreach = valueChange.body().forEach(g, "tmp", values);
		JVar $count1 = foreach.var();
		foreach.body()._if(
			$count1.invoke("toString").invoke("equals").arg(par.invoke("getValue").invoke("toString"))
		)._then().directStatement("return;");
        JVar s = valueChange.body().decl(prj.getCm().ref(Button.class), "btn");
        s.assign(JExpr._new(prj.getCm().ref(Button.class)));
        
        
	}
	
	public static String capitalizeFirst(String s) {
		return s.substring(0,1).toUpperCase() + s.substring(1) ;
	}

}
