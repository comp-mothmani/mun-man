
package com.nogroup.municipality.booster.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "extendedType",
    "implementedTypes",
    "embeddedFields"
})
public class Embedded {

    @JsonProperty("name")
    private String name;
    @JsonProperty("extendedType")
    private String extendedType;
    @JsonProperty("implementedTypes")
    private List<String> implementedTypes = new ArrayList<String>();
    @JsonProperty("embeddedFields")
    private List<EmbeddedField> embeddedFields = new ArrayList<EmbeddedField>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Embedded withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("extendedType")
    public String getExtendedType() {
        return extendedType;
    }

    @JsonProperty("extendedType")
    public void setExtendedType(String extendedType) {
        this.extendedType = extendedType;
    }

    public Embedded withExtendedType(String extendedType) {
        this.extendedType = extendedType;
        return this;
    }

    @JsonProperty("implementedTypes")
    public List<String> getImplementedTypes() {
        return implementedTypes;
    }

    @JsonProperty("implementedTypes")
    public void setImplementedTypes(List<String> implementedTypes) {
        this.implementedTypes = implementedTypes;
    }

    public Embedded withImplementedTypes(List<String> implementedTypes) {
        this.implementedTypes = implementedTypes;
        return this;
    }

    @JsonProperty("embeddedFields")
    public List<EmbeddedField> getEmbeddedFields() {
        return embeddedFields;
    }

    @JsonProperty("embeddedFields")
    public void setEmbeddedFields(List<EmbeddedField> embeddedFields) {
        this.embeddedFields = embeddedFields;
    }

    public Embedded withEmbeddedFields(List<EmbeddedField> embeddedFields) {
        this.embeddedFields = embeddedFields;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Embedded withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(name).append(extendedType).append(implementedTypes).append(embeddedFields).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Embedded) == false) {
            return false;
        }
        Embedded rhs = ((Embedded) other);
        return new EqualsBuilder().append(name, rhs.name).append(extendedType, rhs.extendedType).append(implementedTypes, rhs.implementedTypes).append(embeddedFields, rhs.embeddedFields).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
