package com.nogroup.municipality.booster.builders;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;

import com.nogroup.municipality.booster.models.Project;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JPackage;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;

public class BuildVUI {

	public static void run(Project prj) throws JClassAlreadyExistsException, IOException {
		
		JCodeModel cm = prj.getCm() ;
		JPackage jp = cm._package(prj.rootPackage());
		JDefinedClass jc = jp._class("VUI");
		jc.annotate(Theme.class).param("value", "VTheme");
		jc._extends(UI.class) ;
		
		JMethod initMethod = jc.method(JMod.PROTECTED, cm.VOID, "init") ;
		initMethod.param(VaadinRequest.class, "vaadinRequest") ;
		initMethod.annotate(Override.class) ;
		
		JDefinedClass jc2 = jc._class("VUIServlet") ;
		jc2.annotate(WebServlet.class).param("urlPatterns",  "/*").
			param("name", "VUIServlet").param("asyncSupported", true);
		jc2._extends(VaadinServlet.class);
		jc2.annotate(VaadinServletConfiguration.class).param("ui",jc).param("productionMode", false) ;
		
	}
}
