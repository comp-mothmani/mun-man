package com.nogroup.municipality.booster.builders;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import com.nogroup.municipality.booster.models.Embedded;
import com.nogroup.municipality.booster.models.EmbeddedField;
import com.nogroup.municipality.booster.models.MultiModuleProject;
import com.nogroup.municipality.booster.models.Project;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JFieldVar;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JPackage;

public class BuildEmbeddedTypes implements Serializable {

	public static void run(Project prj) throws JClassAlreadyExistsException, IOException {

		MultiModuleProject mmp = (MultiModuleProject) prj;
		JCodeModel cm = prj.getCm();
		for (Embedded tmp : mmp.getDataTypes().getEmbedded()) {
			JDefinedClass tp = createType(mmp, cm, tmp);
			createCbx(mmp, cm, tp);
		}
	}

	private static void createCbx(MultiModuleProject mmp, JCodeModel cm, JDefinedClass tp) {

	}

	public static String capitalizeFirst(String s) {
		return s.substring(0, 1).toUpperCase() + s.substring(1);
	}

	public static JDefinedClass createType(MultiModuleProject mmp, JCodeModel cm, Embedded tmp)
			throws IOException, JClassAlreadyExistsException {
		// CREATE EMBEDDED TYPE
		JPackage jp = cm._package(mmp.rootPackage() + ".data.embedded");
		JDefinedClass jcc = jp._class(tmp.getName() + "S");

		try {
			if (!tmp.getExtendedType().equals("")) {
				jcc._extends(ResolveDataType.find(tmp.getExtendedType()));
			}
		} catch (Exception e) {
		}

		for (String tmp2 : tmp.getImplementedTypes()) {
			try {
				jcc._implements(ResolveDataType.find(tmp2));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		for (EmbeddedField tmp2 : tmp.getEmbeddedFields()) {
			JFieldVar field = jcc.field(JMod.PRIVATE, ResolveDataType.find(tmp2.getDataType()), tmp2.getName());

			JMethod setter = jcc.method(JMod.PUBLIC, cm.VOID, "set" + capitalizeFirst(field.name()));
			setter.param(field.type(), field.name());
			setter.body().assign(JExpr._this().ref(field.name()), JExpr.ref(field.name()));
			setter.javadoc().add("Set the " + field.name() + ".");
			setter.javadoc().addParam(field.name()).add("the new " + field.name());

			JMethod getter = jcc.method(JMod.PUBLIC, field.type(), "get" + capitalizeFirst(field.name()));
			getter.body()._return(field);
			getter.javadoc().add("Returns the " + field.name() + ".");
			getter.javadoc().addReturn().add(field.name());
		}

		cm.build(new File("src/main/java/"));

		return jcc;
	}
}
