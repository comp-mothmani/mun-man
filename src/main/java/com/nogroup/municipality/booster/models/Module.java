
package com.nogroup.municipality.booster.models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "artifact",
    "version",
    "archetype"
})
@JsonTypeInfo(
  use = JsonTypeInfo.Id.NAME, 
  include = JsonTypeInfo.As.PROPERTY, 
  property = "archetype")
@JsonSubTypes({ 
  @Type(value = GisModule.class, name = "GIS"), 
})
public abstract class Module {

    @JsonProperty("artifact")
    private String artifact;
    @JsonProperty("version")
    private String version;
    @JsonProperty("archetype")
    private String archetype;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("artifact")
    public String getArtifact() {
        return artifact;
    }

    @JsonProperty("artifact")
    public void setArtifact(String artifact) {
        this.artifact = artifact;
    }

    public Module withArtifact(String artifact) {
        this.artifact = artifact;
        return this;
    }

    @JsonProperty("version")
    public String getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    public Module withVersion(String version) {
        this.version = version;
        return this;
    }

    @JsonProperty("archetype")
    public String getArchetype() {
        return archetype;
    }

    @JsonProperty("archetype")
    public void setArchetype(String archetype) {
        this.archetype = archetype;
    }

    public Module withArchetype(String archetype) {
        this.archetype = archetype;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Module withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(artifact).append(version).append(archetype).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Module) == false) {
            return false;
        }
        Module rhs = ((Module) other);
        return new EqualsBuilder().append(artifact, rhs.artifact).append(version, rhs.version).append(archetype, rhs.archetype).append(additionalProperties, rhs.additionalProperties).isEquals();
    }
    
    public abstract void build() ;

}
